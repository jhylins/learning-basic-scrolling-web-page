# Learning How to Create a Basic Scrolling Web Page

This project is being used to improve my knowledge of html and css. I've captured the highlights of what I've learned in the Learning Notes section below.

## Learning Goals

- Explain how html and css can be used to create a simple scrolling page, with a fixed header.

## Design Notes

- I will use only pure html and css, without Javascript.
- Everything will be created from scratch.

## User Stories

### User Story 1

As a user,  
I can see some scrolling content on a web page, with an empty static header block.

### User Story 2

As a user,  
I can see a dynamic header, with a company logo in the header on the left and 2 text links on the right, separted by whitespace.

### User Story 3

As a user,  
I only see the header in a fixed position when I'm on a mobile device with a screen less than 600px wide.

### User Story 4

As a user,  
I know I can use any browser (including IE 11) to see the "sticky" behaviour on the page correctly.

### User Story 5

As a user,  
I can see different articles about cute animals in the main content area. There should be 3 equally sized article boxes, which span the width of the window.

## How to Install

- Clone the project.

```console
$ git clone git@gitlab.com:jhylins/learning-basic-scrolling-web-page.git
```

- Open the index.html file in a browser.

## Learning Notes

### Semantic Elements

For example, `<section>`, `<header>` or `<nav>` used to define structure in a way that is meaningful to screen readers etc. They usually come with default styling.  

### Grouping Elements

`<span>` and `<div>` are both grouping elements, and don't have any special meaning, or default styling. The difference is that span is inline and div is not.  

### CSS Selector (Rules)

There are a lot of ways to select elements via css. They also have use outside css, for example in javascript, so are more important than I first assumed.  
[w3schools demo of selectors](https://www.w3schools.com/cssref/trysel.asp)

### CSS @rules (at-rules)

Special rules, giving instruction to CSS. E.g. @import, @media.  
I've used a @media rule to fix the header only for small screens only.  
I've also used @supports to check if I can use position:sticky.  

### CSS Shorthand Properties

Some CSS properties (e.g. background, margin), allow many property values to be set at once.  
[Mozilla](https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps/How_CSS_is_structured#Shorthands)  

### Flexible Box Layout

I've used a flexible box for the header, to control layout.  

### Float Layout

I've used the float property to lay out the articles, 3 articles wide.  

### CSS Position Property Values

[Guide to Main Values, execpt sticky](https://learnlayout.com/position.html)  

### Other

- html elements have attributes, styles have properties!  
- I've used git tags for the first time to mark when a user story is finished. Note that these don't get pushed to remote by default.  

```console
$ git tag -a v3 -m "user story 3"
$ git push origin --tags
```

- I'm also using semantic commit messages in git to describe the change made.
- The history of the commit snapshots can then be viewed and hopefully is easy to follow. Tags mark the major milestones. I like using the git log command with the oneline option:

```console
$ git log --oneline
```

### Credits & Resources Used

[Beginner's Guide to HTML](https://www.beginnersguidetohtml.com/guides/css/layout/div-tags)  
[w3schools Fixed Menu Guide](https://www.w3schools.com/howto/howto_css_fixed_menu.asp)  
O'Reilly CSS Definitive Guide 4th ed.  
[w3schools How to Build a Website](https://www.w3schools.com/howto/howto_website.asp)  
